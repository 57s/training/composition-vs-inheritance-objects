class Programmer {
	constructor(name) {
		this.name = name;
	}

	sayHello = () => console.log(`Привет меня зовут ${this.name}`);
}

class Frontender extends Programmer {
	constructor({ name }) {
		super(name);
	}

	frontend = () => console.log(`Фронтенд ❤️`);
	work = () => console.log(`Я могу делать фронтенд`);
}

class Backender extends Programmer {
	constructor({ name }) {
		super(name);
	}

	backend = () => console.log(`Бекенд ❤️`);
	work = () => console.log(`Я могу делать бекенд`);
}

// Проблема невозможно унаследоваться от двух классов
// Придется дублировать код или использовать какой либо  костыль
// Подход с композицией объектов решает эту проблему

class FullStack extends Backender {
	constructor({ name }) {
		super({ name });
	}

	work = () => console.log(`Я могу делать бекенд и фронтенд`);
	// Дублирование, не хорошо
	frontend = () => console.log(`Фронтенд ❤️`);
}

{
	const user = new Frontender({ name: 'Loki' });

	console.group('\n Frontender');
	user.sayHello();
	user.work();
	user.frontend();
	console.groupEnd();
}

{
	const user = new Backender({ name: 'Ivar' });

	console.group('\n Backender');
	user.sayHello();
	user.work();
	user.backend();
	console.groupEnd();
}

{
	const user = new FullStack({ name: 'Ragnar' });

	console.group('\n FullStack');
	user.sayHello();
	user.work();
	user.backend();
	user.frontend();
	console.groupEnd();
}
