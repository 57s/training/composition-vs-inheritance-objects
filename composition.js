function CreateProgrammer(name) {
	return {
		name,
		sayHello: () => console.log(`Привет меня зовут ${name}`),
	};
}

function CreateFrontender({ name }) {
	return {
		...CreateProgrammer(name),
		frontend: () => console.log(`Фронтенд ❤️`),
		work: () => console.log(`Я могу делать фронтенд`),
	};
}

function CreateBackender({ name }) {
	return {
		...CreateProgrammer(name),
		backend: () => console.log(`Бекенд ❤️`),
		work: () => console.log(`Я могу делать бекенд`),
	};
}

function CreateFullStack({ name }) {
	return {
		...CreateProgrammer(name),
		...CreateFrontender({ name }),
		...CreateBackender({ name }),
		work: () => console.log(`Я могу делать бекенд и фронтенд`),
	};
}

{
	const user = CreateFrontender({ name: 'Loki' });

	console.group('\n Frontender');
	user.sayHello();
	user.work();
	user.frontend();
	console.groupEnd();
}

{
	const user = CreateBackender({ name: 'Ivar' });

	console.group('\n Backender');
	user.sayHello();
	user.work();
	user.backend();
	console.groupEnd();
}

{
	const user = CreateFullStack({ name: 'Ragnar' });

	console.group('\n FullStack');
	user.sayHello();
	user.work();
	user.backend();
	user.frontend();
	console.groupEnd();
}
